import http from './http'
import type { Receipt } from '@/types/Receipt'
import type { ReceiptItem } from '@/types/RecieptItem'

type ReceiptDto = {
  orderItems: {
    productId: number
    qty: number
    sweet: number
    category: string
  }[]
  promotionId: number
  memberId: number
  userId: number
  memberDiscount: number
  promotionDiscount: number
  change: number
  paymentType: string
  amount: number
  total: number
  totalBefore: number
}
function addOrder(receipt: Receipt, receiptItems: ReceiptItem[]) {
  console.log(receipt)
  console.log(receiptItems)
  const receiptDto: ReceiptDto = {
    orderItems: [],
    promotionId: 0,
    userId: 0,
    memberId: 0,
    memberDiscount: 0,
    promotionDiscount: 0,
    change: 0,
    paymentType: '',
    amount: 0,
    total: 0,
    totalBefore: 0
  }
  receiptDto.promotionId = receipt.promotionId!
  receiptDto.memberId = receipt.memberId!
  receiptDto.userId = receipt.userId
  receiptDto.memberDiscount = receipt.memberDiscount
  receiptDto.promotionDiscount = receipt.promotionDiscount
  receiptDto.change = receipt.change
  receiptDto.paymentType = receipt.paymentType
  receiptDto.amount = receipt.receivedAmount
  receiptDto.totalBefore = receipt.total
  receiptDto.total = receipt.totalNet || 0
  receiptDto.orderItems = (receiptItems as ReceiptItem[]).map((item) => {
    return {
      productId: item.productId as number,
      qty: item.qty,
      sweet: item.sweet,
      category: item.category
    }
  })
  console.log(receiptDto)
  receiptDto.orderItems = receiptDto.orderItems.filter((item) => item.productId !== undefined)
  return http.post('/orders', receiptDto)
}
function getOrders() {
  return http.get('/orders')
}

export default { addOrder, getOrders }
