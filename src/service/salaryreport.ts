import http from './http'
import { type SalaryReport } from '@/types/SalaryReport'
import salary from './salary'

function getSalaryReportperMonth(sale: SalaryReport) {
  return http.post(`/salary-report/reportSalaryPerMonth`, { year: sale.year })
}

function getRoleName(idemp: number) {
  return http.post(`/salary-report/getRoleName`, { employee_Id: idemp })
}

// function getSalaryByEmp(id: number) {
//     return http.post(`/salary-report/salarybyemp`, { employee_Id: id })
// }
export default { getSalaryReportperMonth, getRoleName }
