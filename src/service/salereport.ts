import http from './http'
import { type SaleReport } from '@/types/SaleReport'

function getSaleReportperDayB1(sale: SaleReport) {
  return http.post(`/salereport/reportSaleperDayB1`, { month: sale.month, year: sale.year })
}

function getSaleReportperMonthB1(sale: SaleReport) {
  return http.post(`/salereport/reportSaleperMonthB1`, { year: sale.year })
}

function getSaleReportperYearB1() {
  return http.post(`/salereport/reportSaleperYearB1`)
}

function getSaleReportperDayB2(sale: SaleReport) {
  return http.post(`/salereport/reportSaleperDayB2`, { month: sale.month, year: sale.year })
}

function getSaleReportperMonthB2(sale: SaleReport) {
  return http.post(`/salereport/reportSaleperMonthB2`, { year: sale.year })
}

function getSaleReportperYearB2() {
  return http.post(`/salereport/reportSaleperYearB2`)
}

function getSaleReportperDayB3(sale: SaleReport) {
  return http.post(`/salereport/reportSaleperDayB3`, { month: sale.month, year: sale.year })
}

function getSaleReportperMonthB3(sale: SaleReport) {
  return http.post(`/salereport/reportSaleperMonthB3`, { year: sale.year })
}

function getSaleReportperYearB3() {
  return http.post(`/salereport/reportSaleperYearB3`)
}

export default {
  getSaleReportperDayB1,
  getSaleReportperMonthB1,
  getSaleReportperYearB1,
  getSaleReportperDayB2,
  getSaleReportperMonthB2,
  getSaleReportperYearB2,
  getSaleReportperDayB3,
  getSaleReportperMonthB3,
  getSaleReportperYearB3
}
