import type { StockReceipt } from '@/types/StockReceipt'
import type { StockReceiptItem } from '@/types/StockReceiptItem'
import http from './http'
import type { CheckStock } from '@/types/CheckStock'
import { useAuthStore } from '@/stores/auth'

const authStore = useAuthStore()
type StockReceiptDto = {
  stockReceiptItems: {
    stockId: number
    quantity: number
    price: number
  }[],
  userId: number
}

function getStockReceipt(id: number) {
  return http.get(`stock-receipt/${id}`)
}
function getStockReceipts() {
  return http.get(`stock-receipt`)
}
function addStockReceipt(stockReceipt: StockReceipt, stockReceiptItem: StockReceiptItem[]) {
  const stockReceiptDto: StockReceiptDto = {
    stockReceiptItems: [],
    userId: 0
  }
  stockReceiptDto.userId = authStore.getCurrentUser()?.id!
  stockReceiptDto.stockReceiptItems = stockReceiptItem.map((item) => ({
    stockId: item.stockId,
    quantity: item.quantity,
    price: item.price
  }))
  return http.post('stock-receipt', stockReceiptDto)
}
type checkDate = {
  day?: string
  month?: string
  year?: string
}
function extractDayAndMonth(date: Date): { day: string; year: string; month: string } {
  const year = new Date(date).toLocaleDateString('en-GB', { year: 'numeric' })
  const month = new Date(date).toLocaleDateString('en-GB', { month: '2-digit' })
  const day = new Date(date).toLocaleDateString('en-GB', { day: '2-digit' })
  return { year, month, day }
}
function clearDate(checkDate: checkDate): void {
  checkDate.month = ''
  checkDate.year = ''
}
async function CheckAvg(stockReceipt: StockReceipt) {
  const checkDate: checkDate = {
    month: '',
    year: ''
  }
  checkDate.month = extractDayAndMonth(stockReceipt.receiptDate).month
  checkDate.year = extractDayAndMonth(stockReceipt.receiptDate).year
  console.log(checkDate.month)
  return await http.get(`/stock-receipt/CheckAvg/${checkDate.month}/${checkDate.year}`)
}
function ShowAvg() {
  return http.get(`/stock-receipt/ShowAvg`)
}

async function showStockExpenseReportPermonth(stockReceipt: StockReceipt) {
  const currentUser = authStore.getCurrentUser!()
  const bId = currentUser ? currentUser.branch.id : null
  const checkDate: checkDate = {
    month: '',
    year: ''
  }
  const extractedDate = extractDayAndMonth(stockReceipt.receiptDate)
  checkDate.month = extractedDate.month
  checkDate.year = extractedDate.year
  console.log(checkDate.month)
  console.log(checkDate.year)
  const data = await http.get(
    `/report-stock-receipt/show-stock-expense-report-per-month/${checkDate.month}/${checkDate.year}/${bId}`
  )
  clearDate(checkDate)
  return data
}
async function showStockExpenseReportPermonthForOwnerB1(Date: Date) {
  const checkDate: checkDate = {
    month: '',
    year: ''
  }
  const extractedDate = extractDayAndMonth(Date)
  checkDate.month = extractedDate.month
  checkDate.year = extractedDate.year
  console.log(checkDate.month)
  console.log(checkDate.year)
  const data = await http.get(
    `/report-stock-receipt/show-stock-expense-report-per-month/${checkDate.month}/${checkDate.year}/${1} `
  )
  clearDate(checkDate)
  return data
}
async function showStockExpenseReportPermonthForOwnerB2(Date: Date) {
  const checkDate: checkDate = {
    month: '',
    year: ''
  }
  const extractedDate = extractDayAndMonth(Date)
  checkDate.month = extractedDate.month
  checkDate.year = extractedDate.year
  console.log(checkDate.month)
  console.log(checkDate.year)
  const data = await http.get(
    `/report-stock-receipt/show-stock-expense-report-per-month/${checkDate.month}/${checkDate.year}/${2} `
  )
  clearDate(checkDate)
  return data
}
async function showStockExpenseReportPermonthForOwnerB3(Date: Date) {
  const checkDate: checkDate = {
    month: '',
    year: ''
  }
  const extractedDate = extractDayAndMonth(Date)
  checkDate.month = extractedDate.month
  checkDate.year = extractedDate.year
  console.log(checkDate.month)
  console.log(checkDate.year)
  const data = await http.get(
    `/report-stock-receipt/show-stock-expense-report-per-month/${checkDate.month}/${checkDate.year}/${3} `
  )
  clearDate(checkDate)
  return data
}
async function showStockExpenseReportPerYear(stockReceipt: StockReceipt) {
  const currentUser = authStore.getCurrentUser!()
  const bId = currentUser ? currentUser.branch.id : null
  const checkDate: checkDate = {
    month: '',
    year: ''
  }
  const extractedDate = extractDayAndMonth(stockReceipt.receiptDate)
  checkDate.month = extractedDate.month
  checkDate.year = extractedDate.year
  console.log(checkDate.month)
  console.log(checkDate.year)
  const data = await http.get(
    `/report-stock-receipt/show-stock-total-price-per-year/${checkDate.year}/${bId}`
  )
  clearDate(checkDate)
  return data
}
async function showStockExpenseReportPerYear1(Date: Date) {
  const currentUser = authStore.getCurrentUser!()
  const bId = currentUser ? currentUser.branch.id : null
  const checkDate: checkDate = {
    month: '',
    year: ''
  }
  const extractedDate = extractDayAndMonth(Date)
  checkDate.month = extractedDate.month
  checkDate.year = extractedDate.year
  console.log(checkDate.month)
  console.log(checkDate.year)
  const data = await http.get(
    `/report-stock-receipt/show-stock-total-price-per-year/${checkDate.year}/${1} `
  )
  clearDate(checkDate)
  return data
}
async function showStockExpenseReportPerYear2(Date: Date) {
  const currentUser = authStore.getCurrentUser!()
  const bId = currentUser ? currentUser.branch.id : null
  const checkDate: checkDate = {
    month: '',
    year: ''
  }
  const extractedDate = extractDayAndMonth(Date)
  checkDate.month = extractedDate.month
  checkDate.year = extractedDate.year
  console.log(checkDate.month)
  console.log(checkDate.year)
  const data = await http.get(
    `/report-stock-receipt/show-stock-total-price-per-year/${checkDate.year}/${2} `
  )
  clearDate(checkDate)
  return data
}
async function showStockExpenseReportPerYear3(Date: Date) {
  const currentUser = authStore.getCurrentUser!()
  const bId = currentUser ? currentUser.branch.id : null
  const checkDate: checkDate = {
    month: '',
    year: ''
  }
  const extractedDate = extractDayAndMonth(Date)
  checkDate.month = extractedDate.month
  checkDate.year = extractedDate.year
  console.log(checkDate.month)
  console.log(checkDate.year)
  const data = await http.get(
    `/report-stock-receipt/show-stock-total-price-per-year/${checkDate.year}/${3} `
  )
  clearDate(checkDate)
  return data
}
async function showStockUsedReportPerMonth(checkStockDate: CheckStock) {
  const currentUser = authStore.getCurrentUser!()
  const bId = currentUser ? currentUser.branch.id : null
  const checkDate: checkDate = {
    month: '',
    year: ''
  }
  const extractedDate = extractDayAndMonth(checkStockDate.checkStockDate)
  checkDate.month = extractedDate.month
  checkDate.year = extractedDate.year
  const data = await http.get(
    `/report-check-stock/show-used-stock-per-month/${checkDate.month}/${checkDate.year}/${bId}`
  )
  clearDate(checkDate)
  return data
}
async function showStockUsedReportPerMonthForOwner1(Date: Date) {
  const checkDate: checkDate = {
    month: '',
    year: ''
  }

  const extractedDate = extractDayAndMonth(Date)
  checkDate.month = extractedDate.month
  checkDate.year = extractedDate.year
  const data = await http.get(
    `/report-check-stock/show-used-stock-per-month/${checkDate.month}/${checkDate.year}/${1}`
  )
  clearDate(checkDate)
  return data
}
async function showStockUsedReportPerMonthForOwner2(Date: Date) {
  const checkDate: checkDate = {
    month: '',
    year: ''
  }

  const extractedDate = extractDayAndMonth(Date)
  checkDate.month = extractedDate.month
  checkDate.year = extractedDate.year
  const data = await http.get(
    `/report-check-stock/show-used-stock-per-month/${checkDate.month}/${checkDate.year}/${2}`
  )
  clearDate(checkDate)
  return data
}
async function showStockUsedReportPerMonthForOwner3(Date: Date) {
  const checkDate: checkDate = {
    month: '',
    year: ''
  }

  const extractedDate = extractDayAndMonth(Date)
  checkDate.month = extractedDate.month
  checkDate.year = extractedDate.year
  const data = await http.get(
    `/report-check-stock/show-used-stock-per-month/${checkDate.month}/${checkDate.year}/${3}`
  )
  clearDate(checkDate)
  return data
}
async function showStockRemainPerMonth(checkStockDate: CheckStock) {
  const currentUser = authStore.getCurrentUser!()
  const bId = currentUser ? currentUser.branch.id : null
  const checkDate: checkDate = {
    month: '',
    year: ''
  }
  const extractedDate = extractDayAndMonth(checkStockDate.checkStockDate)
  checkDate.month = extractedDate.month
  checkDate.year = extractedDate.year
  const data = await http.get(
    `/report-check-stock/show-remain-stock-per-month/${checkDate.month}/${checkDate.year}/${bId}`
  )
  clearDate(checkDate)
  return data
}
async function showStockRemainPerMonthForOwner1(Date: Date) {
  const checkDate: checkDate = {
    month: '',
    year: ''
  }
  const extractedDate = extractDayAndMonth(Date)
  checkDate.month = extractedDate.month
  checkDate.year = extractedDate.year
  const data = await http.get(
    `/report-check-stock/show-remain-stock-per-month/${checkDate.month}/${checkDate.year}/${1}`
  )
  console.log()
  clearDate(checkDate)
  return data
}
async function showStockRemainPerMonthForOwner2(Date: Date) {
  const checkDate: checkDate = {
    month: '',
    year: ''
  }
  const extractedDate = extractDayAndMonth(Date)
  checkDate.month = extractedDate.month
  checkDate.year = extractedDate.year
  const data = await http.get(
    `/report-check-stock/show-remain-stock-per-month/${checkDate.month}/${checkDate.year}/${2}`
  )
  clearDate(checkDate)
  return data
}
async function showStockRemainPerMonthForOwner3(Date: Date) {
  const checkDate: checkDate = {
    month: '',
    year: ''
  }
  const extractedDate = extractDayAndMonth(Date)
  checkDate.month = extractedDate.month
  checkDate.year = extractedDate.year
  const data = await http.get(
    `/report-check-stock/show-remain-stock-per-month/${checkDate.month}/${checkDate.year}/${3}`
  )
  clearDate(checkDate)
  return data
}
async function showStockQtyPerMonth(stockReceipt: StockReceipt) {
  const currentUser = authStore.getCurrentUser!()
  const bId = currentUser ? currentUser.branch.id : null
  const checkDate: checkDate = {
    month: '',
    year: ''
  }
  console.log(bId)
  const extractedDate = extractDayAndMonth(stockReceipt.receiptDate)
  checkDate.month = extractedDate.month
  checkDate.year = extractedDate.year
  const data = await http.get(
    `/report-stock-receipt/show-stock-qty-receipt-report-per-month/${checkDate.month}/${checkDate.year}/${bId}`
  )
  clearDate(checkDate)
  return data
}
async function showStockQtyPerMonthForOwner1(Date: Date) {
  const checkDate: checkDate = {
    month: '',
    year: ''
  }
  const extractedDate = extractDayAndMonth(Date)
  checkDate.month = extractedDate.month
  checkDate.year = extractedDate.year
  const data = await http.get(
    `/report-stock-receipt/show-stock-qty-receipt-report-per-month/${checkDate.month}/${checkDate.year}/${1}`
  )
  clearDate(checkDate)
  return data
}
async function showStockQtyPerMonthForOwner2(Date: Date) {
  const checkDate: checkDate = {
    month: '',
    year: ''
  }
  const extractedDate = extractDayAndMonth(Date)
  checkDate.month = extractedDate.month
  checkDate.year = extractedDate.year
  const data = await http.get(
    `/report-stock-receipt/show-stock-qty-receipt-report-per-month/${checkDate.month}/${checkDate.year}/${2}`
  )
  clearDate(checkDate)
  return data
}
async function showStockQtyPerMonthForOwner3(Date: Date) {
  const checkDate: checkDate = {
    month: '',
    year: ''
  }
  const extractedDate = extractDayAndMonth(Date)
  checkDate.month = extractedDate.month
  checkDate.year = extractedDate.year
  const data = await http.get(
    ` /report-stock-receipt/show-stock-qty-receipt-report-per-month/${checkDate.month}/${checkDate.year}/${3}`
  )
  clearDate(checkDate)
  return data
}
async function showStockReportPerDay(stockReceipt: StockReceipt) {
  const currentUser = authStore.getCurrentUser!()
  const bId = currentUser ? currentUser.branch.id : null
  const checkDate: checkDate = {
    day: '',
    month: '',
    year: ''
  }
  const extractedDate = extractDayAndMonth(stockReceipt.receiptDate)
  checkDate.day = extractedDate.day
  checkDate.month = extractedDate.month
  checkDate.year = extractedDate.year

  const data = await http.get(
    `/report-stock-receipt/show-stock-receipt-per-day/${checkDate.year}/${checkDate.month}/${checkDate.day}/${bId}`
  )
  clearDate(checkDate)
  return data
}
async function showStockReportPerDay1(Date: Date) {
  const currentUser = authStore.getCurrentUser!()
  const bId = currentUser ? currentUser.branch.id : null
  const checkDate: checkDate = {
    day: '',
    month: '',
    year: ''
  }
  const extractedDate = extractDayAndMonth(Date)
  checkDate.day = extractedDate.day
  checkDate.month = extractedDate.month
  checkDate.year = extractedDate.year

  const data = await http.get(
    `/report-stock-receipt/show-stock-receipt-per-day/${checkDate.year}/${checkDate.month}/${checkDate.day}/${1}`
  )
  clearDate(checkDate)
  return data
}
async function showStockReportPerDay2(Date: Date) {
  const currentUser = authStore.getCurrentUser!()
  const bId = currentUser ? currentUser.branch.id : null
  const checkDate: checkDate = {
    day: '',
    month: '',
    year: ''
  }
  const extractedDate = extractDayAndMonth(Date)
  checkDate.day = extractedDate.day
  checkDate.month = extractedDate.month
  checkDate.year = extractedDate.year

  const data = await http.get(
    `/report-stock-receipt/show-stock-receipt-per-day/${checkDate.year}/${checkDate.month}/${checkDate.day}/${2}`
  )
  clearDate(checkDate)
  return data
}
async function showStockReportPerDay3(Date: Date) {
  const currentUser = authStore.getCurrentUser!()
  const bId = currentUser ? currentUser.branch.id : null
  const checkDate: checkDate = {
    day: '',
    month: '',
    year: ''
  }
  const extractedDate = extractDayAndMonth(Date)
  checkDate.day = extractedDate.day
  checkDate.month = extractedDate.month
  checkDate.year = extractedDate.year

  const data = await http.get(
    `/report-stock-receipt/show-stock-receipt-per-day/${checkDate.year}/${checkDate.month}/${checkDate.day}/${3}`
  )
  clearDate(checkDate)
  return data
}

export default {
  addStockReceipt,
  getStockReceipt,
  getStockReceipts,
  CheckAvg,
  ShowAvg,
  showStockExpenseReportPermonth,
  showStockExpenseReportPerYear,
  showStockUsedReportPerMonth,
  showStockRemainPerMonth,
  showStockQtyPerMonth,
  showStockReportPerDay,
  showStockExpenseReportPermonthForOwnerB1,
  showStockExpenseReportPermonthForOwnerB2,
  showStockExpenseReportPermonthForOwnerB3,
  showStockUsedReportPerMonthForOwner1,
  showStockUsedReportPerMonthForOwner2,
  showStockUsedReportPerMonthForOwner3,
  showStockRemainPerMonthForOwner1,
  showStockRemainPerMonthForOwner2,
  showStockRemainPerMonthForOwner3,
  showStockQtyPerMonthForOwner1,
  showStockQtyPerMonthForOwner2,
  showStockQtyPerMonthForOwner3,
  showStockExpenseReportPerYear1,
  showStockExpenseReportPerYear2,
  showStockExpenseReportPerYear3,
  showStockReportPerDay1,
  showStockReportPerDay2,
  showStockReportPerDay3
}
