import type { Expense } from '@/types/Expense'
import http from './http'

function addExpense(expense: Expense) {
  return http.post('/expenses', expense)
}

function updateExpense(expense: Expense) {
  return http.patch(`/expenses/${expense.id}`, expense)
}

function delExpense(expense: Expense) {
  return http.delete(`/expenses/${expense.id}`)
}

function getExpense(id: number) {
  return http.get(`/expenses/${id}`)
}

function getExpenses() {
  return http.get(`/expenses`)
}

type checkDate = {
  month?: string
  year?: string
}

function extractDayAndMonth(date: Date): { year: string; month: string } {
  const year = new Date(date).toLocaleDateString('en-GB', { year: 'numeric' })
  const month = new Date(date).toLocaleDateString('en-GB', { month: '2-digit' })
  return { year, month }
}

async function showMonthExp(expense: Expense) {
  const checkDate: checkDate = {
    month: '',
    year: ''
  }
  checkDate.month = extractDayAndMonth(expense.createDate).month
  checkDate.year = extractDayAndMonth(expense.createDate).year
  return await http.get(`/expenses/showMonthExp/${checkDate.month}/${checkDate.year}`)
}

async function showYearExp(expense: Expense) {
  const checkDate: checkDate = {
    year: ''
  }

  checkDate.year = extractDayAndMonth(expense.createDate).year
  return await http.get(`/expenses/showYearExp/${checkDate.year}`)
}

export default {
  addExpense,
  updateExpense,
  delExpense,
  getExpense,
  getExpenses,
  showMonthExp,
  showYearExp
}
