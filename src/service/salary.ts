import employee from './employee'
import http from './http'
import { type Salary } from '@/types/Salary'

function addSalary(salary: Salary) {
  console.log('call addSalary from service')
  console.log(salary)
  return http.post('/salarys', salary)
}

function updateSalary(salary: Salary) {
  console.log('call update salary from service...')
  console.log(salary)
  return http.patch(`/salarys/${salary.id}`, salary)
}

function delSalary(salary: Salary) {
  return http.delete(`/salarys/${salary.id}`)
}

function getSalary(id: number) {
  return http.get(`/salarys/${id}`)
}
function getSalarys() {
  return http.get('/salarys')
}
// function getSalarys() {
//   console.log(localStorage.getItem('access_token'))
//   return http.get('/salarys', {
//     headers: {
//       Authorization: 'Bearer ' + localStorage.getItem('access_token')
//     }
//   })
// }

function getSalaryByEmpId(empId: number) {
  return http.get('/salarys/empId/' + empId)
}

export default { addSalary, updateSalary, delSalary, getSalary, getSalarys, getSalaryByEmpId }
