import type { Promotion } from '@/types/Promotion'
import http from './http'

function addPromotion(promotion: Promotion & { files: File[] }) {
  const fromData = new FormData()
  fromData.append('name', promotion.name)
  fromData.append('condition', promotion.condition)
  fromData.append('percentDiscount', promotion.percentDiscount.toString())
  fromData.append('priceDiscount', promotion.priceDiscount.toString())
  fromData.append('minQty', promotion.minQty.toString())
  fromData.append('minPrice', promotion.minPrice.toString())
  fromData.append('start_date', promotion.start_date)
  fromData.append('end_date', promotion.end_date)
  fromData.append('member', promotion.member.toString())
  fromData.append('image', promotion.image)
  if (promotion.files && promotion.files.length > 0) fromData.append('file', promotion.files[0])
  console.log(fromData)
  return http.post('/promotions', fromData, {
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
}

function updatePromotion(promotion: Promotion & { files: File[] }) {
  const fromData = new FormData()
  fromData.append('name', promotion.name)
  fromData.append('condition', promotion.condition)
  fromData.append('percentDiscount', promotion.percentDiscount.toString())
  fromData.append('priceDiscount', promotion.priceDiscount.toString())
  fromData.append('minQty', promotion.minQty.toString())
  fromData.append('minPrice', promotion.minPrice.toString())
  fromData.append('start_date', promotion.start_date)
  fromData.append('end_date', promotion.end_date)
  fromData.append('member', promotion.member.toString())
  fromData.append('image', promotion.image)
  if (promotion.files && promotion.files.length > 0) fromData.append('file', promotion.files[0])
  console.log(fromData)
  return http.post(`/promotions/${promotion.id}`, fromData, {
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
}

function delPromotion(promotion: Promotion) {
  return http.delete(`/promotions/${promotion.id}`)
}

function getPromotion(id: number) {
  return http.get(`/promotions/${id}`)
}

function getPromotions() {
  return http.get('/promotions')
}

function getPromotionsByType(typeId: number) {
  return http.get('/promotions/type/' + typeId)
}

export default {
  addPromotion,
  updatePromotion,
  delPromotion,
  getPromotion,
  getPromotions,
  getPromotionsByType
}
