import type { StockReceipt } from '@/types/StockReceipt'
import http from './http'

type checkDate = {
  month?: string
  year?: string
}

function extractDayAndMonth(date: Date): { year: string; month: string } {
  const year = new Date(date).toLocaleDateString('en-GB', { year: 'numeric' })
  const month = new Date(date).toLocaleDateString('en-GB', { month: '2-digit' })
  return { year, month }
}

async function showStockExpenseReportPermonth(stockReceipt: StockReceipt): Promise<any> {
  const checkDate: checkDate = {
    month: '',
    year: ''
  }
  const extractedDate = extractDayAndMonth(stockReceipt.receiptDate)
  checkDate.month = extractedDate.month
  checkDate.year = extractedDate.year
  console.log(http.get(`show-stock-expense-report-per-month/${checkDate.month}/${checkDate.year}`))
  return await http.get(`show-stock-expense-report-per-month/${checkDate.month}/${checkDate.year}`)
}

export default { showStockExpenseReportPermonth }
