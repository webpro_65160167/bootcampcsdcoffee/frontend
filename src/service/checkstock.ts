import type { CheckStock } from '@/types/CheckStock'
import http from './http'
import type { CheckStockItem } from '@/types/CheckStockItem'

type CheckStockDto = {
  checkStockItems: {
    stockId: number
    remain: number
  }[]
  userId: number
}
type checkDate = {
  month?: string
  year?: string
}
function getCheckStock(id: number) {
  return http.get(`/check-stock/${id}`)
}

function getCheckStocks() {
  return http.get('/check-stock')
}
function extractDayAndMonth(date: Date): { year: string; month: string } {
  const year = new Date(date).toLocaleDateString('en-GB', { year: 'numeric' })
  const month = new Date(date).toLocaleDateString('en-GB', { month: '2-digit' })
  return { year, month }
}
function addCheckStock(orderReceipt: CheckStock, checkStockItems: CheckStockItem[]) {
  const CheckStockDto: CheckStockDto = {
    checkStockItems: [],
    userId: 0
  }
  console.log(orderReceipt)
  CheckStockDto.userId = orderReceipt.employeeId
  CheckStockDto.checkStockItems = checkStockItems.map((item) => {
    return {
      stockId: item.stockId,
      remain: item.remain
    }
  })
  return http.post('/check-stock', CheckStockDto)
}
async function showStatusStock(checkstock: CheckStock) {
  const checkDate: checkDate = {
    month: '',
    year: ''
  }
  checkDate.month = extractDayAndMonth(checkstock.checkStockDate).month
  checkDate.year = extractDayAndMonth(checkstock.checkStockDate).year
  return await http.get(`/check-stock/showstatus/${checkDate.month}/${checkDate.year}`)
}
export default { addCheckStock, getCheckStock, getCheckStocks, showStatusStock }
