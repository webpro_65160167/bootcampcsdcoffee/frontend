import type { Gender } from './User'
type Role = 'barista' | 'cashier'
type User = {
  id: number
  name: string
  gender: Gender
  tel: string
  username: string
  password: string
  role: Role
  // timein: Date | string
  // timeout: Date | string
  // timework: number
  bankAccount: string
}
export type { User, Gender, Role }
