import type { Member } from './Member'
import type { Promotion } from './Promotion'
import type { ReceiptItem } from './RecieptItem'
import type { User } from './User'

type Receipt = {
  id: number
  created: Date
  qty: number
  total: number
  totalNet: number
  memberDiscount: number
  promotionDiscount: number
  receivedAmount: number
  change: number
  paymentType: string
  userId: number
  user?: User
  memberId: number
  member?: Member
  promotionId?: number
  promotion?: Promotion
  receiptItems?: ReceiptItem[]
}

export type { Receipt }
