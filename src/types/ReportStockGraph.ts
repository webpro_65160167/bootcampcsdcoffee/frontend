type ExpenseReportPermonth = {
  MONTH: number
  name: String
  total: number
}
type ExpenseReportPerYear = {
  MONTH: number
  total: number
}
type UsedReportPerMonth = {
  MONTH: number
  name: string
  TotalUsed: number
}
type RemainReportPerMonth = {
  checkStockDate: string
  name: string
  remain: number
}
type showStockReceiptPerDay = {
  receiptDate: string
  name: string
  quantity: number
  price: number
  total_value: number
}
type QtyReportPerMonth = {
  month: number
  name: string
  totalQuantity: string
}
export {
  type ExpenseReportPermonth,
  type ExpenseReportPerYear,
  type UsedReportPerMonth,
  type RemainReportPerMonth,
  type QtyReportPerMonth,
  type showStockReceiptPerDay
}
