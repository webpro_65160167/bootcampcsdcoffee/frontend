import type { Branch } from './Branch'

type Stock = {
  id?: number
  name: string
  priceUnit: number
  quantity: number
  value: number
  stock: boolean
  userId: number
  branch: Branch
}
export { type Stock }
