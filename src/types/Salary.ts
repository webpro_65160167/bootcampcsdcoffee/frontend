import type { User } from './User'
type status = 'Paid' | 'Pending'
type Salary = {
  id: number
  employee: User
  employee_Id: number
  employee_Name: string
  employee_BankAcc: string
  baseSalary: number
  increaseSalary: number
  salaryReduc: number
  netSalary: number
  timework: number
  payDate: Date
  status: status
  roleName: string
}
export { type Salary, type status }
