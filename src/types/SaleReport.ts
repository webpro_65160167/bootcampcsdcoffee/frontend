type SaleReport = {
  month: string
  year: string
}
type SaleReportResult = {
  order_date: string
  order_month: string
  order_year: string
  total_sum: number
}
export { type SaleReport, type SaleReportResult }
