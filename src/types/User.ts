import type { Branch } from './Branch'
import type { Role } from './Role'
type Gender = 'Male' | 'Female' | 'Others'
type User = {
  id: number
  email: string
  password: string
  fullName: string
  image: string
  gender: Gender // Male, Female, Others
  roles: Role[] // admin, user
  bankAcc: string
  timework: number
  tel: string
  branch: Branch
}

export type { Gender, User }
