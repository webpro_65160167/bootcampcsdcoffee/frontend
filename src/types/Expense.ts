import type { Branch } from './Branch'

type Expense = {
  id?: number
  name: string
  branchName?: Branch
  billType: string
  totalAmount: number
  createDate: Date
  note: string
}
export type { Expense }
