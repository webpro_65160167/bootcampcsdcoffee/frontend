import type { Stock } from './Stock'

type CheckStockItem = {
  id: number
  name: string
  lastCheckQty: number
  used: number
  remain: number
  stockId: number
  stock?: Stock
}

export { type CheckStockItem }
