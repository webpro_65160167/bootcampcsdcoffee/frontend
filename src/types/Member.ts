type Member = {
  id: number
  name: string
  tel: string
  point: number
  password: string
}

export type { Member }
