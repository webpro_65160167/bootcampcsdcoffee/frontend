import type { Product } from './Product'

const defaultReceiptItem = {
  id: -1,
  name: '',
  price: 0,
  qty: 0,
  category: 'hot',
  sweet: 100,
  productId: -1,
  product: null
}
type ReceiptItem = {
  id: number
  name: string
  price: number
  qty: number
  category: string
  sweet: number
  productId?: number
  product?: Product
}

export { type ReceiptItem, defaultReceiptItem }
