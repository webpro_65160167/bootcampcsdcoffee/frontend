type SalaryReport = {
  month: string
  year: string
}
type SalaryReportResult = {
  salary_date: string
  salary_month: string
  salary_year: string
  total_sum: number
}
export { type SalaryReport, type SalaryReportResult }
