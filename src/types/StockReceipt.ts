import type { StockReceiptItem } from './StockReceiptItem'
import type { User } from './User'

type StockReceipt = {
  id: number
  receiptDate: Date
  totalPrice?: number
  totalQuantity?: number
  employeeName: string
  employeeId: number
  stockReceiptItems?: StockReceiptItem[]
  employee: User
}
export { type StockReceipt }
