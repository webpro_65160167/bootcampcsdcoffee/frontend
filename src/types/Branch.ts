type Branch = {
  id?: number
  name: string
  address: string
  email: string
  tel: string
  openDate: string
}

export type { Branch }
