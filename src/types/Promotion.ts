type Promotion = {
  id?: number
  name: string
  condition: string
  percentDiscount: number
  priceDiscount: number
  minQty: number
  minPrice: number
  start_date: string
  image: string
  member: boolean
  end_date: string
}
function getImageUrl(promotion: Promotion) {
  return `http://localhost:3000/images/promotions/${promotion.image}`
}
export { type Promotion, getImageUrl }
