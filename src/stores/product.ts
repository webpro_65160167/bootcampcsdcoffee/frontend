import { useLoadingStore } from './loading'
import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import productService from '@/service/product'
import type { Product } from '@/types/Product'
import { useMessageStore } from './massage'

export const useProductStore = defineStore('product', () => {
  const loadingStore = useLoadingStore()
  const messageStore = useMessageStore()
  const products = ref<Product[]>([])
  const product = ref<Product>()
  const initialProduct: Product & { files: File[] } = {
    name: '',
    price: 0,
    type: { id: 2, name: 'drinks' },
    image: 'noimage.png',
    category: '',
    sweet: 100,
    files: []
  }
  const editedProduct = ref<Product & { files: File[] }>(JSON.parse(JSON.stringify(initialProduct)))
  initProduct()

  function initProduct() {
    product.value = {
      name: '',
      price: 0,
      type: { id: 2, name: 'bekery' },
      category: '',
      sweet: 100,
      image: 'noimage.png'
    }
  }
  async function getProduct(id: number) {
    try {
      loadingStore.doLoad()
      const res = await productService.getProduct(id)
      editedProduct.value = res.data
      loadingStore.finish()
    } catch (e: any) {
      loadingStore.finish()
      messageStore.showMessage(e.message)
    }
  }
  async function getProducts() {
    loadingStore.doLoad()
    const res = await productService.getProducts()
    products.value = res.data
    loadingStore.finish()
  }
  async function saveProduct() {
    try {
      loadingStore.doLoad()
      const product = editedProduct.value
      if (!product.id) {
        // Add new
        console.log('Post ' + JSON.stringify(product))
        const res = await productService.addProduct(product)
      } else {
        // Update
        console.log('Patch ' + JSON.stringify(product))
        const res = await productService.updateProduct(product)
      }

      await getProducts()
      loadingStore.finish()
    } catch (e: any) {
      messageStore.showMessage(e.message)
      loadingStore.finish()
    }
  }
  function addProduct(p: Product) {
    product.value = p
    console.log('addProduct' + p)
  }
  async function deleteProduct() {
    loadingStore.doLoad()
    const product = editedProduct.value
    const res = await productService.delProduct(product)

    await getProducts()
    loadingStore.finish()
  }

  function updateProduct(p: Product) {
    product.value = p
    console.log('updateProduct' + product.value)
  }

  function changCategory(category: number, product: Product) {
    if (category === 1) {
      product.category = 'hot'
      updateProduct(product)
    } else if (category === 2) {
      product.category = 'iced'
      updateProduct(product)
    } else if (category === 3) {
      product.category = 'blended'
      updateProduct(product)
    }
  }

  function addSweet(sweet: number, product: Product) {
    console.log(sweet)
    if (sweet === 0) {
      product.sweet = 0
    } else if (sweet === 50) {
      product.sweet = 50
    } else if (sweet === 100) {
      product.sweet = 100
    } else if (sweet === 150) {
      product.sweet = 150
    }
    console.log(product)

    updateProduct(product) // แก้ไขเป็นการส่งค่า product.value ที่เป็น Object ที่มีค่า sweet ที่ถูกแก้ไขแล้ว
  }

  function clearForm() {
    editedProduct.value = JSON.parse(JSON.stringify(initialProduct))
  }
  function clear() {
    product.value = {
      name: '',
      price: 0,
      type: { id: 2, name: 'drink' },
      category: '',
      sweet: 100,
      image: 'noimage.png'
    }
  }
  return {
    products,
    product,
    editedProduct,
    getProducts,
    saveProduct,
    deleteProduct,
    getProduct,
    clearForm,
    addProduct,
    clear,
    changCategory,
    addSweet
  }
})
