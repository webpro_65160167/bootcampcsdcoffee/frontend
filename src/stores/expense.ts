import type { Expense } from '@/types/Expense'
import { useLoadingStore } from './loading'
import expenseService from '@/service/expense'

import { defineStore } from 'pinia'
import { ref } from 'vue'

export const useExpenseStore = defineStore('expense', () => {
  const loadingStore = useLoadingStore()
  const expenses = ref<Expense[]>([])
  const expensesData = ref<Expense[]>([])
  const expenseYear = ref<Expense[]>([])
  const initialExpense: Expense = {
    name: '',
    createDate: new Date(),
    // branchName: '',
    billType: '',
    totalAmount: 0,
    note: '-'
  }
  const editedExpense = ref<Expense>(JSON.parse(JSON.stringify(initialExpense)))

  async function getExpense(id: number) {
    loadingStore.doLoad()
    const res = await expenseService.getExpense(id)
    editedExpense.value = res.data
    loadingStore.finish()
  }
  //Data function

  async function getExpenses() {
    try {
      loadingStore.doLoad()
      const res = await expenseService.getExpenses()
      expenses.value = res.data
      console.log(res.data)
      loadingStore.finish()
    } catch (e) {
      loadingStore.finish()
    }
  }

  async function saveExpense() {
    loadingStore.doLoad()
    const expense = editedExpense.value
    console.log(expense.branchName)
    try {
      if (!expense.id) {
        //Add new
        console.log('Post ' + JSON.stringify(expense))
        const res = await expenseService.addExpense(expense)
      } else {
        // Update
        console.log('Patch ' + JSON.stringify(expense))
        const res = await expenseService.updateExpense(expense)
      }
      await getExpenses()
    } catch (error) {
      console.error('Error saving expense:', error)
    } finally {
      loadingStore.finish()
    }

    //  catch (e) {
    //   // messageStore.showMessage(e.message)
    //   loadingStore.finish()
    //   console.error(e)
    // }
  }

  async function deleteExpense() {
    loadingStore.doLoad()
    const expense = editedExpense.value
    const res = await expenseService.delExpense(expense)

    await getExpenses()
    loadingStore.finish()
  }

  function clearForm() {
    editedExpense.value = JSON.parse(JSON.stringify(initialExpense))
  }

  async function monthExpense(expense: Expense) {
    try {
      loadingStore.doLoad()
      const res = await expenseService.showMonthExp(expense)
      console.log(res.data)
      expenses.value = res.data
      expensesData.value = res.data
      loadingStore.finish()
    } catch (error) {
      loadingStore.finish()
    }
  }

  async function yearExpense(expense: Expense) {
    try {
      loadingStore.doLoad()
      const res = await expenseService.showYearExp(expense)
      console.log(res.data)
      expenses.value = res.data
      expenseYear.value = res.data
      loadingStore.finish()
    } catch (error) {
      loadingStore.finish()
    }
  }

  function filterExpensesByBranch(branchAddress: string) {
    // Filter expenses based on branch address
    expensesData.value = expensesData.value.filter(
      (expense) => expense.branchName?.address === branchAddress
    )
  }
  return {
    expenses,
    getExpense,
    getExpenses,
    saveExpense,
    deleteExpense,
    clearForm,
    editedExpense,
    monthExpense,
    expensesData,
    expenseYear,
    yearExpense,
    filterExpensesByBranch
  }
})
