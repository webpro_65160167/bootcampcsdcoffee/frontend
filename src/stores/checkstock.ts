import { defineStore } from 'pinia'
import { useLoadingStore } from './loading'
import { ref, watch } from 'vue'
import { useAuthStore } from './auth'
import type { Stock } from '@/types/Stock'
import type { CheckStockItem } from '@/types/CheckStockItem'
import type { CheckStock } from '@/types/CheckStock'
import CheckStockService from '@/service/checkstock'
import checkstock from '@/service/checkstock'

export const useCheckStockStore = defineStore('checkStock', () => {
  const loadingStore = useLoadingStore()
  const authStore = useAuthStore()
  const checkStockItems = ref<CheckStockItem[]>([])
  const checkStocks = ref<CheckStock>()
  const checkStocksData = ref<CheckStock[]>([])
  const initialCheckStockItems: CheckStockItem = {
    id: 0,
    name: '',
    lastCheckQty: 0,
    used: 0,
    remain: 0,
    stockId: -1,
    stock: null!
  }
  const initialCheckStock: CheckStock = {
    id: 0,
    checkStockDate: new Date(),
    checkStockAmount: 0,
    employeeId: 0,
    employeeName: '',
    employee: null!,
    checkStockItems: []!
  }

  // initCheckStock()
  // function initCheckStock() {
  //   checkStocks.value = {
  //     id: -1,
  //     checkStockDate: new Date(),
  //     checkStockAmount: 0,
  //     employeeId: authStore.getCurrentUser()!.id!,
  //     employeeName: authStore.getCurrentUser()!.fullName,
  //     employee: authStore.getCurrentUser()!
  //   }
  //   checkStockItems.value = []
  // }

  const editedCheckStockItem = ref<CheckStockItem>(
    JSON.parse(JSON.stringify(initialCheckStockItems))
  )
  const editedCheckStock = ref<CheckStock>(JSON.parse(JSON.stringify(initialCheckStock)))

  async function getCheckStock(id: number) {
    try {
      loadingStore.doLoad()
      const res = await CheckStockService.getCheckStock(id)
      editedCheckStock.value = res.data[0]
      loadingStore.finish()
    } catch {
      loadingStore.finish()
    }
  }
  async function showStatusStock(checkStock: CheckStock) {
    try {
      loadingStore.doLoad()
      const res = await CheckStockService.showStatusStock(checkStock)
      checkStocks.value = res.data
      checkStocksData.value = res.data
      loadingStore.finish()
    } catch {
      loadingStore.finish()
    }
  }

  async function getCheckStocks() {
    try {
      loadingStore.doLoad()
      const res = await CheckStockService.getCheckStocks()
      //   console.log(res)
      checkStocks.value = res.data
      checkStocksData.value = res.data
      loadingStore.finish()
    } catch {
      loadingStore.finish()
    }
  }

  async function saveCheckStock() {
    try {
      loadingStore.doLoad()
      checkStocks.value = {
        id: -1,
        checkStockDate: new Date(),
        employeeId: authStore.getCurrentUser()!.id!,
        employeeName: authStore.getCurrentUser()!.fullName,
        employee: authStore.getCurrentUser()!
      }
      // getCheckStocks()
      await CheckStockService.addCheckStock(checkStocks.value!, checkStockItems.value,)
      getCheckStocks()
      loadingStore.finish()
    } catch (e: any) {
      loadingStore.finish()
    }
  }
  watch(
    checkStockItems,
    () => {
      calculate()
    },
    { deep: true }
  )
  async function addCheckStockItems(s: Stock) {
    const newCheckStock: CheckStockItem = {
      id: -1,
      name: s.name,
      lastCheckQty: s.quantity,
      remain: editedCheckStockItem.value.remain,
      used: s.quantity - editedCheckStockItem.value.remain,
      stockId: s.id!,
      stock: s
    }
    if (newCheckStock.used < 0) return
    for (const item of checkStockItems.value) {
      if (item.name.toUpperCase() === newCheckStock.name.toUpperCase()) return
    }
    checkStockItems.value.push(newCheckStock)
    clearCheckStockItems()
  }

  const calculate = function () {
    checkStocks.value!.checkStockAmount = 0
    for (const item of checkStockItems.value) {
      checkStocks.value!.checkStockAmount += item.used
    }
  }

  function clearCheckStock() {
    checkStockItems.value = []
    checkStocks.value = {
      id: -1,
      checkStockDate: new Date(),
      checkStockAmount: 0,
      employeeId: authStore.getCurrentUser()!.id!,
      employeeName: authStore.getCurrentUser()!.fullName,
      employee: authStore.getCurrentUser()!
    }
  }

  function clearCheckStockItems() {
    editedCheckStockItem.value.id = -1
    editedCheckStockItem.value.name = ''
    editedCheckStockItem.value.lastCheckQty = 0
    editedCheckStockItem.value.remain = 0
    editedCheckStockItem.value.used = 0
  }

  async function removeCheckStockItems(checkStockItem: CheckStockItem) {
    const index = checkStockItems.value.findIndex((item) => item === checkStockItem)
    checkStockItems.value.splice(index, 1)
  }
  return {
    checkStockItems,
    checkStocks,
    checkStocksData,
    editedCheckStock,
    editedCheckStockItem,
    getCheckStock,
    getCheckStocks,
    removeCheckStockItems,
    clearCheckStock,
    clearCheckStockItems,
    addCheckStockItems,
    saveCheckStock,
    showStatusStock
  }
})
