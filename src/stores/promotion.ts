import { useLoadingStore } from './loading'
import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import promotionService from '@/service/promotion'
import type { Promotion } from '@/types/Promotion'
import { useMessageStore } from './massage'
import { useReceiptStore } from './receipt'
import product from '@/service/product'

export const usePromotionStore = defineStore('promotion', () => {
  const loadingStore = useLoadingStore()
  const messageStore = useMessageStore()
  const recieptStore = useReceiptStore()
  const promotions = ref<Promotion[]>([])
  const discount = ref(0)
  const promotionDialog = ref(false)
  const initialPromotion: Promotion & { files: File[] } = {
    name: '',
    condition: '',
    percentDiscount: 0,
    priceDiscount: 0,
    minQty: 0,
    minPrice: 0,
    start_date: '',
    end_date: '',
    image: 'noimage.png',
    member: false,
    files: []
  }
  const editedPromotion = ref<Promotion & { files: File[] }>(
    JSON.parse(JSON.stringify(initialPromotion))
  )

  async function getPromotion(id: number) {
    try {
      loadingStore.doLoad()
      const res = await promotionService.getPromotion(id)
      editedPromotion.value = res.data
      loadingStore.finish()
    } catch (e: any) {
      loadingStore.finish()
      messageStore.showMessage(e.message)
    }
  }
  async function getPromotions() {
    loadingStore.doLoad()
    const res = await promotionService.getPromotions()
    promotions.value = res.data
    loadingStore.finish()
  }
  async function savePromotion() {
    try {
      loadingStore.doLoad()
      const promotion = editedPromotion.value
      if (!promotion.id) {
        // Add new
        console.log('Post ' + JSON.stringify(promotion))
        const res = await promotionService.addPromotion(promotion)
      } else {
        // Update
        console.log('Patch ' + JSON.stringify(promotion))
        const res = await promotionService.updatePromotion(promotion)
      }

      await getPromotions()
      loadingStore.finish()
    } catch (e: any) {
      messageStore.showMessage(e.message)
      loadingStore.finish()
    }
  }
  async function deletePromotion() {
    loadingStore.doLoad()
    const promotion = editedPromotion.value
    const res = await promotionService.delPromotion(promotion)
    await getPromotions()
    loadingStore.finish()
  }

  function checkstate(promotion: Promotion) {
    const currentDate = new Date()
    const startDate = new Date(promotion.start_date)
    const endDate = new Date(promotion.end_date)
    const hasMember = recieptStore.receipt.member

    if (startDate <= currentDate && currentDate <= endDate) {
      // กรณีอยู่ระหว่างช่วง promotion
      if (promotion.member && hasMember) {
        // ถ้า promotion กำหนดให้มี member และ receipt มี member
        return true
      } else if (!promotion.member) {
        // ถ้า promotion ไม่กำหนดให้มี member
        return true
      }
    }
    return false
  }

  function usePromotoin(id: number) {
    getPromotion(id)
    if (editedPromotion.value.condition === 'priceDiscount') {
      discount.value = editedPromotion.value.priceDiscount
      console.log()
    } else if (editedPromotion.value.condition === 'percentDiscount') {
      discount.value = editedPromotion.value.percentDiscount / 100
      console.log()
    }
  }

  function clearForm() {
    editedPromotion.value = JSON.parse(JSON.stringify(initialPromotion))
  }
  return {
    promotionDialog,
    discount,
    promotions,
    editedPromotion,
    checkstate,
    usePromotoin,
    getPromotions,
    savePromotion,
    deletePromotion,
    getPromotion,
    clearForm
  }
})
