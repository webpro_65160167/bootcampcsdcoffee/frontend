import { ref } from 'vue'
import { defineStore } from 'pinia'
import { useLoadingStore } from './loading'
import salaryService from '@/service/salary'
import http from '@/service/http'
import type { Salary } from '@/types/Salary'
import { useMessageStore } from './massage'
import salaryreport from '@/service/salaryreport'

export const useSalaryStore = defineStore('salary', () => {
  const salarys = ref<Salary[]>([])
  const loadingStore = useLoadingStore()
  const messageStore = useMessageStore()
  const initialSalary: Salary = {
    id: -1,
    employee: {
      id: -1,
      fullName: '',
      email: '',
      password: '',
      image: '',
      gender: 'male',
      roles: [],
      bankAcc: '',
      timework: 0,
      tel: ''
    },
    employee_Id: -1,
    employee_Name: '',
    employee_BankAcc: '',
    baseSalary: 0,
    increaseSalary: 0,
    salaryReduc: 0,
    netSalary: 0,
    timework: 0, //checkworkStore.employee[1].timework
    payDate: new Date(),
    status: 'Pending',
    roleName: ''
  }
  const editedSalary = ref<Salary>(JSON.parse(JSON.stringify(initialSalary)))

  async function getSalarys() {
    loadingStore.doLoad()
    const res = await salaryService.getSalarys()
    salarys.value = res.data
    console.log(res)
    console.log(salarys.value)
    loadingStore.finish()
  }
  async function getSalary(id: number | any) {
    loadingStore.doLoad()
    const res = await salaryService.getSalary(id)
    editedSalary.value = res.data
    loadingStore.finish()
  }
  async function saveSalary() {
    // loadingStore.doLoad()
    // const salary = editedSalary.value
    console.log('call saveSalary...')
    // console.log(salary)
    //add new
    try {
      loadingStore.doLoad()
      const salary = editedSalary.value
      if (salary.id === -1) {
        salary.id = salarys.value.length + 1
        salary.employee_Id = salary.employee.id //assign empId in salary
        salary.employee_Name = salary.employee.fullName
        salary.employee_BankAcc = salary.employee.bankAcc
        // Add new
        console.log('Post ' + JSON.stringify(salary))
        const res = await salaryService.addSalary(salary)
      } else {
        // Update
        console.log('Patch ' + JSON.stringify(salary))
        const res = await salaryService.updateSalary(salary)
        console.log(res)
      }

      await getSalarys()
      loadingStore.finish()
    } catch (e: any) {
      messageStore.showMessage(e.message)
      loadingStore.finish()
    }
  }
  async function deleteSalary() {
    loadingStore.doLoad()
    const salary = editedSalary.value
    const res = await salaryService.delSalary(salary)
    await getSalarys()
    loadingStore.finish()
  }
  function clearForm() {
    editedSalary.value = JSON.parse(JSON.stringify(initialSalary))
  }
  async function paySalary() {
    loadingStore.doLoad()
    const salary = editedSalary.value
    salary.status = 'Paid'
    const res = await salaryService.updateSalary(salary)
    console.log('Call PaySalary...')
    console.log('Patch ' + JSON.stringify(salary))
    await getSalarys()
    loadingStore.finish()
  }

  function getStatusById(id: number): string | null {
    const salary = salarys.value.find((salary) => salary.id === id)
    return salary ? salary?.status : null
  }

  async function getSalaryByEmp(id: number) {
    const res = await salaryreport.getRoleName(id)
    editedSalary.value.roleName = res.data
    console.log('cal getSalaryByEmp')
    return editedSalary.value.roleName
  }

  return {
    getSalary,
    getSalarys,
    saveSalary,
    deleteSalary,
    clearForm,
    salarys,
    editedSalary,
    paySalary,
    getStatusById,
    getSalaryByEmp
  }
})
