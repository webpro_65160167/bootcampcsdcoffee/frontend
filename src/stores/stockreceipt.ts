import type { StockReceipt } from '@/types/StockReceipt'
import type { StockReceiptItem } from '@/types/StockReceiptItem'
import { defineStore } from 'pinia'
import { ref, watch } from 'vue'
import StockReceiptService from '@/service/stockReceipt'
import { useLoadingStore } from './loading'
import type { Stock } from '@/types/Stock'
import { useAuthStore } from './auth'
import {
  type ExpenseReportPerYear,
  type ExpenseReportPermonth,
  type QtyReportPerMonth,
  type RemainReportPerMonth,
  type UsedReportPerMonth,
  type showStockReceiptPerDay
} from '@/types/ReportStockGraph'
import type { CheckStock } from '@/types/CheckStock'
import employee from '@/service/employee'
export const useStockReceiptStore = defineStore('stock-receipt', () => {
  const loadingStore = useLoadingStore()
  const authStore = useAuthStore()
  const stockReceiptItems = ref<StockReceiptItem[]>([])
  const stockReceipts = ref<StockReceipt>()
  const stockReceiptsData = ref<StockReceipt[]>([])
  const stockReceiptsDa = ref<StockReceipt[]>([])

  const stockReceiptsGraph = ref<ExpenseReportPermonth[]>([])
  const stockReceiptsGraph1 = ref<ExpenseReportPerYear[]>([])
  const stockReceiptsGraph2 = ref<UsedReportPerMonth[]>([])
  const stockReceiptsGraph3 = ref<RemainReportPerMonth[]>([])
  const stockReceiptsGraph4 = ref<QtyReportPerMonth[]>([])
  const stockReceiptsGraph5 = ref<showStockReceiptPerDay[]>([])
  const initialStockReceiptItems: StockReceiptItem = {
    id: -1,
    name: '',
    price: 0,
    quantity: 0,
    value: 0,
    stockId: -1,
    stock: null!
  }
  const initialStockReceipts: StockReceipt = {
    id: 0,
    receiptDate: new Date(),
    totalQuantity: 0,
    totalPrice: 0,
    employeeId: 0,
    employeeName: '',
    stockReceiptItems: []!,
    employee: authStore.getCurrentUser()!
  }

  initOrderReceipt()
  function initOrderReceipt() {
    stockReceipts.value = {
      id: -1,
      receiptDate: new Date(),
      totalQuantity: 0,
      totalPrice: 0,
      employeeId: authStore.getCurrentUser()!.id!,
      employeeName: authStore.getCurrentUser()!.fullName,
      employee: authStore.getCurrentUser()!
    }
    stockReceiptItems.value = []
  }

  const editedStockReceiptItem = ref<StockReceiptItem>(
    JSON.parse(JSON.stringify(initialStockReceiptItems))
  )
  const editedStockReceipt = ref<StockReceipt>(JSON.parse(JSON.stringify(initialStockReceipts)))

  async function getStockReceipt(id: number) {
    try {
      loadingStore.doLoad()
      const res = await StockReceiptService.getStockReceipt(id)
      editedStockReceipt.value = res.data[0]
      loadingStore.finish()
    } catch (error) {
      loadingStore.finish()
      console.error(error)
    }
  }
  async function getStockReceipts() {
    try {
      loadingStore.doLoad()
      const res = await StockReceiptService.getStockReceipts()
      stockReceipts.value = res.data
      stockReceiptsData.value = res.data
      loadingStore.finish()
    } catch (error) {
      loadingStore.finish()
      console.error(error)
    }
  }
  async function CheckAvg(stockReceipt: StockReceipt) {
    try {
      loadingStore.doLoad()
      const res = await StockReceiptService.CheckAvg(stockReceipt)
      stockReceipts.value = res.data
      stockReceiptsData.value = res.data

      loadingStore.finish()
    } catch (error) {
      loadingStore.finish()
      console.error(error)
    }
  }

  async function ShowAvg() {
    try {
      loadingStore.doLoad()
      const res = await StockReceiptService.ShowAvg()

      stockReceipts.value = res.data
      stockReceiptsDa.value = res.data

      loadingStore.finish()
    } catch (error) {
      loadingStore.finish()
      console.error(error)
    }
  }
  async function saveStockReceipt() {
    try {
      loadingStore.doLoad()
      stockReceipts.value = {
        id: -1,
        receiptDate: new Date(),
        employeeId: authStore.getCurrentUser()!.id!,
        employeeName: authStore.getCurrentUser()!.fullName,
        employee: authStore.getCurrentUser()!
      }
      await StockReceiptService.addStockReceipt(stockReceipts.value!, stockReceiptItems.value)
      getStockReceipts()
      loadingStore.finish()
    } catch (error) {
      console.error(error)
      loadingStore.finish()
    }
  }

  const calculate = function () {
    stockReceipts.value!.totalQuantity = 0
    stockReceipts.value!.totalPrice = 0
    for (const item of stockReceiptItems.value) {
      stockReceipts.value!.totalPrice += item.value
      stockReceipts.value!.totalQuantity += item.quantity
    }
  }
  async function addStockReceiptItems(s: Stock) {
    const newStockReceipt: StockReceiptItem = {
      id: -1,
      name: s.name,
      price: editedStockReceiptItem.value.price,
      quantity: editedStockReceiptItem.value.quantity,
      value: editedStockReceiptItem.value.price * editedStockReceiptItem.value.quantity,
      stockId: s.id!,
      stock: s
    }
    if (newStockReceipt.quantity <= 0 || newStockReceipt.price <= 0) return
    for (const item of stockReceiptItems.value) {
      if (item.name.toUpperCase() === newStockReceipt.name.toUpperCase()) return
    }
    stockReceiptItems.value.push(newStockReceipt)
    clearStockReceiptItems()
  }
  function clearStockReceiptItems() {
    ; (editedStockReceiptItem.value.id = -1),
      (editedStockReceiptItem.value.name = ''),
      (editedStockReceiptItem.value.price = 0),
      (editedStockReceiptItem.value.quantity = 0),
      (editedStockReceiptItem.value.value = 0)
  }
  function clearStockReceipt() {
    stockReceiptItems.value = []
    stockReceipts.value = {
      id: -1,
      receiptDate: new Date(),
      totalPrice: 0,
      totalQuantity: 0,
      employeeName: '',
      employeeId: 0
    }
  }
  function removeStockReceiptItem(stockReceiptItem: StockReceiptItem) {
    const index = stockReceiptItems.value.findIndex((item) => item == stockReceiptItem)
    stockReceiptItems.value.splice(index, 1)
  }

  async function showStockExpenseReportPermonth(stockReceipt: StockReceipt) {
    try {
      const res = await StockReceiptService.showStockExpenseReportPermonth(stockReceipt)
      // stockReceipts.value = res.data
      stockReceiptsGraph.value = res.data
    } catch (error) {
      console.error(error)
    }
  }
  async function showStockExpenseReportPerYear(stockReceipt: StockReceipt) {
    try {
      const res = await StockReceiptService.showStockExpenseReportPerYear(stockReceipt)
      // stockReceipts.value = res.data
      stockReceiptsGraph1.value = res.data

    } catch (error) {
      console.error(error)
    }
  }
  async function showStockUsedReportPerMonth(checkStockDate: CheckStock) {
    try {
      const res = await StockReceiptService.showStockUsedReportPerMonth(checkStockDate)
      // stockReceipts.value = res.data
      stockReceiptsGraph2.value = res.data

    } catch (error) {
      console.error(error)
    }
  }
  async function showStockRemainPerMonth(checkStockDate: CheckStock) {
    try {
      const res = await StockReceiptService.showStockRemainPerMonth(checkStockDate)
      // stockReceipts.value = res.data
      stockReceiptsGraph3.value = res.data

    } catch (error) {
      console.error(error)
    }
  }
  async function showStockQtyPerMonth(stockReceipt: StockReceipt) {
    try {
      const res = await StockReceiptService.showStockQtyPerMonth(stockReceipt)
      // stockReceipts.value = res.data
      stockReceiptsGraph4.value = res.data

    } catch (error) {
      console.error(error)
    }
  }
  async function showStockReportPerDay(stockReceipt: StockReceipt) {
    try {
      const res = await StockReceiptService.showStockReportPerDay(stockReceipt)
      // stockReceipts.value = res.data
      stockReceiptsGraph5.value = res.data
    } catch (error) {
      console.error(error)
    }
  }
  return {
    stockReceiptsDa,
    stockReceipts,
    stockReceiptItems,
    stockReceiptsData,
    editedStockReceipt,
    editedStockReceiptItem,
    stockReceiptsGraph,
    stockReceiptsGraph1,
    stockReceiptsGraph2,
    stockReceiptsGraph3,
    stockReceiptsGraph4,
    stockReceiptsGraph5,
    clearStockReceipt,
    clearStockReceiptItems,
    addStockReceiptItems,
    saveStockReceipt,
    getStockReceipt,
    getStockReceipts,
    removeStockReceiptItem,
    CheckAvg,
    ShowAvg,
    showStockExpenseReportPermonth,
    showStockExpenseReportPerYear,
    showStockUsedReportPerMonth,
    showStockRemainPerMonth,
    showStockQtyPerMonth,
    showStockReportPerDay
  }
})
