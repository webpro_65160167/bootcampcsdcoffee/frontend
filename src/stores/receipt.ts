import { ref, watch, type Ref } from 'vue'
import { defineStore } from 'pinia'
import type { ReceiptItem } from '@/types/RecieptItem'
import type { Receipt } from '@/types/Receipt'
import { useAuthStore } from './auth'
import { useMemberStore } from './member'
import { useLoadingStore } from './loading'
import { useMessageStore } from './massage'
import orderService from '@/service/order'
import { type Product } from '@/types/Product'
import type { Promotion } from '@/types/Promotion'
import { usePromotionStore } from './promotion'
import memberService from '@/service/member'

export const useReceiptStore = defineStore('receipt', () => {
  const memberStore = useMemberStore()
  const promotionStore = usePromotionStore()
  const authStore = useAuthStore()
  const loadingStore = useLoadingStore()
  const messageStore = useMessageStore()
  const qrDialog = ref(false)
  const erDialog = ref(false)
  const errorCashDialog = ref(false)
  const receiptDialog = ref(false)
  const receiptPaperDialog = ref(false)
  const receiptPaperHistoryDialog = ref(false)
  const paymentType = ref<string>('')
  const receipt = ref<Receipt>({
    id: 0,
    created: new Date(),
    total: 0,
    totalNet: 0,
    qty: 0,
    memberDiscount: 0,
    promotionDiscount: 0,
    receivedAmount: 0,
    change: 0,
    paymentType: '',
    userId: authStore.getCurrentUser()!.id!,
    user: authStore.getCurrentUser()!,
    memberId: 0,
    promotionId: 0
  })
  const receiptItems = ref<ReceiptItem[]>([])
  const Orders = ref<Receipt[]>([])

  const Order = ref<Receipt>({
    id: 0,
    created: new Date(),
    total: 0,
    totalNet: 0,
    qty: 0,
    memberDiscount: 0,
    promotionDiscount: 0,
    receivedAmount: 0,
    change: 0,
    paymentType: '',
    userId: authStore.getCurrentUser()!.id!,
    user: authStore.getCurrentUser()!,
    memberId: 0,
    promotionId: 0
  })
  initReceipt()
  function initReceipt() {
    receipt.value = {
      id: 0,
      created: new Date(),
      total: 0,
      totalNet: 0,
      qty: 0,
      memberDiscount: 0,
      promotionDiscount: 0,
      receivedAmount: 0,
      change: 0,
      paymentType: '',
      userId: authStore.getCurrentUser()!.id!,
      user: authStore.getCurrentUser()!,
      memberId: 0,
      promotionId: 0
    }
    receiptItems.value = []
  }
  watch(
    receiptItems,
    () => {
      calReceipt()
    },
    { deep: true }
  )
  async function getOrder(id: number) {
    try {
      loadingStore.doLoad()

      const selectedReceipt = Orders.value.findIndex((receipt) => receipt.id === id)
      if (selectedReceipt != -1) {
        Order.value = Orders.value[selectedReceipt]
        console.log(Order.value)
        receiptPaperHistoryDialog.value = true
      }
      loadingStore.finish()
    } catch (e: any) {
      messageStore.showMessage(e.message)
      loadingStore.finish()
    }
  }

  async function getOrders() {
    try {
      loadingStore.doLoad()
      const res = await orderService.getOrders()
      Orders.value = res.data
      console.log(res.data)
      loadingStore.finish()
    } catch (e) {
      loadingStore.finish()
    }
  }
  function setPaymentType(type: string) {
    paymentType.value = type
  }
  function calReceipt() {
    let total = 0
    let qty = 0
    for (const item of receiptItems.value) {
      total += item.price * item.qty
      qty += item.qty
    }
    receipt.value!.qty = qty
    receipt.value!.total = total
    receipt.value!.totalNet = total
    const memberDiscount = memberStore.editedMember ? receipt.value!.memberDiscount : 0
    const promotionDiscount = promotionStore.editedPromotion ? receipt.value!.promotionDiscount : 0
    receipt.value!.totalNet -= memberDiscount + promotionDiscount
    receipt.value!.change = receipt.value!.receivedAmount - receipt.value!.totalNet
  }
  function calculateChange(): number {
    const receivedAmount = receipt.value!.receivedAmount || 0
    const total = receipt.value!.totalNet || 0
    const change = receivedAmount - total
    receipt.value!.change = change
    return change
  }

  async function addReceiptItem(product: Product) {
    const index = receiptItems.value.findIndex(
      (item) =>
        item.productId === product.id &&
        item.sweet === product.sweet &&
        item.category === product.category
    )
    if (index >= 0) {
      receiptItems.value[index].qty++
    } else {
      let price = product.price
      if (product.category === 'cool') {
        price += 5.0
      } else if (product.category === 'blended') {
        price += 10.0
      }
      const newReceiptItem: ReceiptItem = {
        id: -1,
        name: product.name,
        price: price,
        qty: 1,
        category: product.category,
        sweet: product.sweet,
        productId: product.id
      }
      receipt.value.promotionDiscount = 0
      await receiptItems.value.push(newReceiptItem)
      console.log(receiptItems.value)
    }
    calReceipt()
  }

  async function addPromotion(promotion: Promotion) {
    console.log(promotion)
    let price
    const currentDate = new Date()
    const startDate = new Date(promotion.start_date)
    const endDate = new Date(promotion.end_date)
    console.log(startDate)
    console.log(currentDate)
    console.log(endDate)
    if (startDate <= currentDate && currentDate <= endDate) {
      if (promotion.condition === 'priceDiscount') {
        if (promotion.minQty <= receipt.value.qty && promotion.minPrice <= receipt.value.total) {
          price = promotion.priceDiscount
          receipt.value.promotionId = promotion.id!
          receipt.value.promotionDiscount = price
          receipt.value.totalNet -= receipt.value.promotionDiscount
        }
      } else if (promotion.condition === 'percentDiscount') {
        if (promotion.minQty <= receipt.value.qty && promotion.minPrice <= receipt.value.total) {
          price = promotion.percentDiscount / 100
          receipt.value.promotionId = promotion.id!
          receipt.value.promotionDiscount = price * receipt.value.totalNet
          receipt.value.totalNet -= receipt.value.promotionDiscount
        }
      }
    }
    console.log(receipt.value.promotionDiscount)
    console.log(receipt.value.totalNet)
    calReceipt()
  }

  async function hidePaymentTypeDialog() {
    const receivedAmount = receipt.value?.receivedAmount || 0
    const total = receipt.value?.totalNet || 0
    const change = receivedAmount - total
    const incPoint = 0
    if (paymentType.value === 'cash') {
      if (change >= 0) {
        receipt.value!.paymentType = 'cash'
        receiptDialog.value = false
        receiptPaperDialog.value = true
        if (receipt.value.memberId != 0) {
          const incPoint = receipt.value.totalNet / 10
          console.log(incPoint)
          memberStore.updateMemberPoint(
            receipt.value.memberId,
            memberStore.editedMember.point + incPoint
          )
        }
        order()
      } else {
        alert('กรุณาใส่จำนวนเงินให้ถูกต้อง')
      }
    } else if (paymentType.value === 'qrcode') {
      receipt.value!.paymentType = 'qrcode'
      receipt.value!.receivedAmount = receipt.value!.totalNet
      receipt.value!.change = 0
      receiptDialog.value = false
      receiptPaperDialog.value = true
      if (receipt.value.memberId != 0) {
        const incPoint = receipt.value.totalNet / 10
        console.log(incPoint)
        memberStore.updateMemberPoint(
          receipt.value.memberId,
          memberStore.editedMember.point + incPoint
        )
      }
      order()
    } else {
      alert('กรุณาเลือกช่องทางการชำระเงิน')
    }
  }
  function showReceiptDialog() {
    receipt.value!.receiptItems = receiptItems.value
    receiptDialog.value = true
  }
  const showReceipt = function () {
    if (receiptItems.value.length > 0) {
      receipt.value!.created = new Date()
      showReceiptDialog()
      receipt.value!.receivedAmount = 0
    } else {
      alert('กรุณาเลือกสินค้าก่อน')
    }
  }
  const deleteReceiptItem = (selectedItem: ReceiptItem) => {
    const index = receiptItems.value.findIndex((item) => item === selectedItem)
    receiptItems.value.splice(index, 1)
  }
  const incUnitOfReceiptItem = (selectedItem: ReceiptItem) => {
    selectedItem.qty++
  }
  const decUnitOfReceiptItem = (selectedItem: ReceiptItem) => {
    selectedItem.qty--
    if (selectedItem.qty === 0) {
      deleteReceiptItem(selectedItem)
    }
    if (
      promotionStore.editedPromotion.minQty >= receipt.value.qty ||
      promotionStore.editedPromotion.minPrice >= receipt.value.total
    ) {
      receipt.value.promotionDiscount = 0
    }
    calReceipt()
  }
  const removeItem = (item: ReceiptItem) => {
    const index = receiptItems.value.findIndex((ri) => ri === item)
    receiptItems.value.splice(index, 1)
    if (
      promotionStore.editedPromotion.minQty >= receipt.value.qty ||
      promotionStore.editedPromotion.minPrice >= receipt.value.total
    ) {
      receipt.value.promotionDiscount = 0
    }
    calReceipt()
  }

  const order = async () => {
    cleanUp()
    try {
      loadingStore.doLoad()
      await orderService.addOrder(receipt.value!, receiptItems.value)
      loadingStore.finish()
    } catch (e: any) {
      loadingStore.finish()
      messageStore.showMessage(e.message)
    }
  }
  async function clear() {
    console.log('clear')
    await memberStore.clearForm()
    await memberStore.clearSearch()
    receipt.value!.id = 0
    ;(receipt.value!.totalNet = 0),
      (receipt.value!.promotionDiscount = 0),
      (receipt.value!.memberDiscount = 0),
      (receipt.value!.total = 0),
      (receipt.value!.receivedAmount = 0),
      (receipt.value!.change = 0),
      (receipt.value!.paymentType = ''),
      (receipt.value!.memberId = 0),
      (receipt.value!.promotionId = 0),
      (receipt.value!.member = undefined),
      (receipt.value!.user = authStore.getCurrentUser()!)
    receiptItems.value = []
    console.log(receipt.value)
  }

  let element: any
  const ppQrCode = ref('')
  function generatePromptPay() {
    const ppUrl = 'https://promptpay.io/'
    const phoneNumber = '0957328288'
    const amount = receipt.value?.totalNet

    ppQrCode.value = ppUrl + phoneNumber + '.png' + '/' + amount

    element = document.getElementById('pp_url')

    if (!element) {
      const img = document.createElement('img')
      img.id = 'pp_url'
      document.body.appendChild(img)
      element = img
    }
    element.setAttribute('src', ppQrCode.value)
  }
  function cleanUp() {
    // ลบรหัส QR และอิลิเมนต์รูปภาพ
    ppQrCode.value = ''
    if (element) {
      element.parentNode.removeChild(element)
    }
  }

  return {
    element,
    ppQrCode,
    Orders,
    receiptItems,
    receipt,
    Order,
    paymentType,
    receiptPaperDialog,
    receiptPaperHistoryDialog,
    qrDialog,
    errorCashDialog,
    erDialog,
    receiptDialog,
    addPromotion,
    hidePaymentTypeDialog,
    showReceiptDialog,
    generatePromptPay,
    showReceipt,
    calculateChange,
    setPaymentType,
    addReceiptItem,
    incUnitOfReceiptItem,
    calReceipt,
    getOrders,
    getOrder,
    order,
    decUnitOfReceiptItem,
    removeItem,
    clear,
    initReceipt
  }
})
