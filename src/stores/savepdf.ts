import { defineStore } from 'pinia'
import jsPDF from 'jspdf'
import html2canvas from 'html2canvas'
import { useReceiptStore } from './receipt'

export const useSavePDFStore = defineStore('savepdf', () => {
  const recieptStore = useReceiptStore()
  function saveRecriptAsPDF() {
    const dialog = document.querySelector('.v-card') as HTMLElement
    if (!dialog) return

    html2canvas(dialog, { scale: 2 }).then((canvas) => {
      const imgData = canvas.toDataURL('image/jpeg', 1.0) // ใช้ JPEG เพื่อความคมชัด
      const pdf = new jsPDF({
        orientation: 'p',
        unit: 'pt',
        format: [canvas.width, canvas.height]
      })

      // คำนวณขนาดของรูปภาพที่จะแทรกลงใน PDF
      const imgWidth = pdf.internal.pageSize.getWidth()
      const imgHeight = (canvas.height * imgWidth) / canvas.width

      pdf.addImage(imgData, 'JPEG', 0, 0, imgWidth - 100, imgHeight)
      pdf.save(`receipt${recieptStore.receipt.created}.pdf`)
    })
  }
  return { saveRecriptAsPDF }
})
