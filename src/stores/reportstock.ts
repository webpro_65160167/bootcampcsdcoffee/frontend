import { defineStore } from 'pinia'
import { useLoadingStore } from './loading'
import { ref } from 'vue'
import ReportStockService from '@/service/reportstock'
import type { StockReceipt } from '@/types/StockReceipt'
import type { StockReceiptItem } from '@/types/StockReceiptItem'

export const useReportStockStore = defineStore('report-check-stock', () => {
  const loadingStore = useLoadingStore()
  const stockReceiptItems = ref<StockReceiptItem[]>([])
  const stockReceipts = ref<StockReceipt>()
  const stockReceiptsData = ref<StockReceipt[]>([])
  const stockReceiptsDa = ref<StockReceipt[]>([])

  return {
    stockReceiptItems,
    stockReceiptsDa,
    stockReceiptsData,
    stockReceipts
  }
})
